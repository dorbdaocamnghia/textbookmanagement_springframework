/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pnv.MainController;

import com.pnv.mappingBook.BookDaoImpl;
import com.pnv.mappingBook.BookclassDaoImpl;
import com.pnv.mappingBook.CategoriesDaoImpl;
import com.pnv.model.Bookclass;
import com.pnv.model.Number;
import com.pnv.model.Books;
import com.pnv.model.Categories;
import com.pnv.util.BookValidation;
import com.pnv.util.Constant;
import java.util.List;
import javax.validation.Valid;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

/**
 *
 * @author dorA
 */
@Controller
@RequestMapping(value = "book")
public class BookController {

    private BookDaoImpl bookDao;
    private CategoriesDaoImpl cateDao;
    private BookclassDaoImpl bookclDao;

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String viewWelcomePage(ModelMap map) {
        bookDao = new BookDaoImpl();
        List<Books> book_list = bookDao.findAll();
        map.put("book_list", book_list);
        int m = bookDao.getCount();
        Number num = new Number();
        num.setN(m);
        map.put("num", num);
        return "phanTrang";
    }
    @RequestMapping(value = "/update", method = RequestMethod.GET)
    public String viewEditBook(@RequestParam(value = "id", required = true) String id, ModelMap map) {
        cateDao = new CategoriesDaoImpl();
        bookDao = new BookDaoImpl();
        bookclDao = new BookclassDaoImpl();
        List<Categories> cate_list = cateDao.findAll();
        map.addAttribute("cate_list", cate_list);
        List<Bookclass> cateForm = bookclDao.findAll();
        map.addAttribute("bookclass_list", cateForm);
        List<Books> book_list = bookDao.findAll();
        map.addAttribute("book_list", book_list);
        Books bookForm = bookDao.findByBooktId(id);
        map.addAttribute("bookForm", bookForm);
        return "book-add";
    }

    @RequestMapping(value = "/add", method = RequestMethod.GET)
    public String addNewBook(ModelMap map) {
        cateDao = new CategoriesDaoImpl();
        bookclDao = new BookclassDaoImpl();
        List<Categories> cate_list = cateDao.findAll();
        map.addAttribute("cate_list", cate_list);
        List<Bookclass> cateForm = bookclDao.findAll();
        map.addAttribute("bookclass_list", cateForm);

        map.addAttribute("bookForm", new Books());

        return "book-add";

    }

    @RequestMapping(value = "/save", method = RequestMethod.POST)
    public String doAddNew(@Valid @ModelAttribute("bookForm") Books bookForm,
            BindingResult result, ModelMap map) {
        bookDao = new BookDaoImpl();
        BookValidation bookvalidate = new BookValidation();
        bookvalidate.validate(bookForm, result);
        if (result.hasErrors()) {
            List<Categories> cate_list = cateDao.findAll();
            map.addAttribute("cate_list", cate_list);
            List<Bookclass> cateForm = bookclDao.findAll();
            map.addAttribute("bookclass_list", cateForm);
            map.addAttribute("bookForm", bookForm);
            return "book-add";
        }

        bookDao.saveOrUpdate(bookForm);
        bookForm.getClass();
        /**
         * Get all titles
         */
        List<Books> book_list = bookDao.findAll();
        map.put("book_list", book_list);
        int m = bookDao.getCount();
        Number num = new Number();
        num.setN(m);
        map.put("num", num);
        return "phanTrang";
    }

    @RequestMapping(value = "/findBook", method = RequestMethod.GET)
    public String finBook(@RequestParam(value = "id", required = true) String id, ModelMap map) {
        bookDao = new BookDaoImpl();
        List<Books> bookForm = bookDao.findByBookIdClass(id);
        map.addAttribute("book_list", bookForm);
        int m = bookDao.getCount();
        Number num = new Number();
        num.setN(m);
        map.put("num", num);
        return "phanTrang";
    }
    @RequestMapping(value = "/delete", method = RequestMethod.GET)
    public String doDeleteTitle(@RequestParam(value = "id", required = true) String id, ModelMap map) {

        bookDao.delete(bookDao.findByBooktId(id));
        return Constant.REDIRECT + "/book/";

    }
    @RequestMapping(value = "/seach", method = RequestMethod.POST)
    public String searchingBook(@RequestParam(value = "select", required = true) String select,
    @RequestParam(value = "search", required = true) String search,ModelMap map) {
        bookDao = new BookDaoImpl();
        if (select.equals("id")) {
            List<Books> bookForm = bookDao.searchByID(search);
            map.addAttribute("book_list", bookForm);
        }
        else if(select.equals("class")) {
            List<Books> bookForm = bookDao.searchByClass(search);
            map.addAttribute("book_list", bookForm);
        }else{
            List<Books> bookForm = bookDao.searchByName(search);
            map.addAttribute("book_list", bookForm);
        }
        int m = bookDao.getCount();
        Number num = new Number();
        num.setN(m);
        map.put("num", num);
        return "phanTrang";
    }
}
