/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pnv.MainController;

import com.pnv.mappingBook.BookDaoImpl;
import com.pnv.mappingBook.UserDaoImpl;
import com.pnv.model.Books;
import com.pnv.model.User;
import java.util.ArrayList;
import java.util.List;
import javax.validation.Valid;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

/**
 *
 * @author bdaoy
 */
@Controller
@RequestMapping(value = "/")
public class UserController {

    private UserDaoImpl us;
    private BookDaoImpl bookDao;

    @RequestMapping(method = RequestMethod.GET)
    public String checkLogin() {
        return "Login";
    }

    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public String validate(@RequestParam("username") String username,
            @RequestParam("password") String pass, ModelMap map) {
        us = new UserDaoImpl();
        boolean check = us.checkUser(username, pass);
        if (check == true) {
            bookDao = new BookDaoImpl();
            List<Books> book_list = bookDao.findAll();
            map.put("book_list", book_list);
            int m = bookDao.getCount();
            com.pnv.model.Number num = new com.pnv.model.Number();
            num.setN(m);
            map.put("num", num);
            return "phanTrang";
        } else {
            map.addAttribute("errorLogin", "Your username or password is wrong !");
            return "Login";
        }
    }

    @RequestMapping(value = "/change", method = RequestMethod.GET)
    public String changePass(ModelMap map) {
        us = new UserDaoImpl();
        User changeForm = us.findByUserId("bdaoCamDorNghia");
        map.addAttribute("user", changeForm.getUsername());
        map.addAttribute("changeForm", changeForm);
        return "changePass";
    }

    @RequestMapping(value = "/change", method = RequestMethod.POST)
    public String doChangePass(@Valid @ModelAttribute("bookForm") User bookForm, @RequestParam("username1") String username, @RequestParam("newpass") String pass,
            @RequestParam("confirmpassword") String confirmpassword, @RequestParam("oldpass") String oldpass,
            BindingResult result, ModelMap map) {
        us = new UserDaoImpl();
        boolean check = us.checkUser(username, oldpass);
        if (check == true) {
            if (pass.toString().equals(confirmpassword)) {
                User user = new User();
                user.setPassword(pass);
                user.setUsername(username);
                us.saveOrUpdate(user);
                bookDao = new BookDaoImpl();
                List<Books> book_list = bookDao.findAll();
                map.put("book_list", book_list);
                int m = bookDao.getCount();
                com.pnv.model.Number num = new com.pnv.model.Number();
                num.setN(m);
                map.put("num", num);
                return "phanTrang";
            } else {

                User changeForm = us.findByUserId("bdaoCamDorNghia");
                map.addAttribute("user", changeForm.getUsername());
                map.addAttribute("changeForm", changeForm);
                map.addAttribute("errorCP", "Confirm password is wrong !");
                return "changePass";

            }
        } else {
            User changeForm = us.findByUserId("bdaoCamDorNghia");
            map.addAttribute("user", changeForm.getUsername());
            map.addAttribute("changeForm", changeForm);
            map.addAttribute("errorOldP", "Your old password is worng !");
            return "changePass";
        }
    }
}
