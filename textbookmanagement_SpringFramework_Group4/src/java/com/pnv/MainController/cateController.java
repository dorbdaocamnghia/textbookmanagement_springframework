/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pnv.MainController;

import com.pnv.mappingBook.CategoriesDaoImpl;
import com.pnv.model.Categories;
import java.util.List;
import javax.validation.Valid;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author dorA
 */
@Controller
@RequestMapping(value = "cate")
public class cateController {
    private CategoriesDaoImpl cateDao;

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String viewCate(ModelMap map) {
        cateDao = new CategoriesDaoImpl();
        List<Categories> cate_list = cateDao.findAll();
        map.put("cate_list", cate_list);
        return "homeCate";
    }
    @RequestMapping(value = "/add", method = RequestMethod.GET)
    public String viewAddNewCate(ModelMap map) {

        Categories cateForm = new Categories();
        map.addAttribute("cateForm", cateForm);
        return "cate-add";

    }

    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public String doAddNew(@Valid @ModelAttribute("cateForm") Categories cateForm,
            BindingResult result, ModelMap map) {
        cateDao = new CategoriesDaoImpl();
        if (result.hasErrors()) {
            map.addAttribute("titleForm", cateForm);
            return "cate-add";
        }
        cateDao.saveOrUpdate(cateForm);

        /**
         * Get all titles
         */
        List<Categories> cate_list = cateDao.findAll();
        map.put("cate_list", cate_list);
        return "homeCate";
    }
    
}
