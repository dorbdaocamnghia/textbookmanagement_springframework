/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pnv.MainController;


import com.pnv.mappingBook.BookclassDaoImpl;
import com.pnv.model.Bookclass;
import java.util.List;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author dorA
 */
@Controller
@RequestMapping(value = "class")
public class ClassController {
    private BookclassDaoImpl classDao;

    @RequestMapping(method = RequestMethod.GET)
    public String viewCate(ModelMap map) {
        classDao = new BookclassDaoImpl();
        List<Bookclass> class_list = classDao.findAll();
        map.put("class_list", class_list);
        return "homeClass";
    }
}
