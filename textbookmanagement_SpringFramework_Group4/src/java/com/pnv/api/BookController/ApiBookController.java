/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pnv.api.BookController;

import com.pnv.mappingBook.BookDaoImpl;
import com.pnv.model.Books;
import java.util.List;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author dorA
 */
@Controller
@RequestMapping(value = "api")
public class ApiBookController {
    private BookDaoImpl bookDao;

    @RequestMapping(value = "/allBook",method = RequestMethod.GET)
    public  @ResponseBody List<Books> viewBookPage() {
        /**
         * Get all titles
         */
        bookDao = new BookDaoImpl();
        List<Books> book_list = bookDao.findAll();
        return book_list;
    }
    @RequestMapping(value = "/class", method = RequestMethod.GET)
    public @ResponseBody List<Books> finBook(@RequestParam(value = "id", required = true) String id, ModelMap map) {
        bookDao = new BookDaoImpl();
        List<Books> bookForm = bookDao.findByBookIdClass(id);
        return bookForm;
    }
}
