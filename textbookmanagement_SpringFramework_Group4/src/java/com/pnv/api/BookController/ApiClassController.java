/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pnv.api.BookController;

import com.pnv.mappingBook.BookclassDaoImpl;
import com.pnv.model.Bookclass;
import java.util.List;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author dorA
 */
@Controller
@RequestMapping(value = "apiclass")
public class ApiClassController {
    private BookclassDaoImpl classDa0;
    @RequestMapping(method = RequestMethod.GET)
    public  @ResponseBody List<Bookclass> viewClass() {
        classDa0 = new BookclassDaoImpl();
        List<Bookclass> class_list = classDa0.findAll();
        return class_list;
    }
}
