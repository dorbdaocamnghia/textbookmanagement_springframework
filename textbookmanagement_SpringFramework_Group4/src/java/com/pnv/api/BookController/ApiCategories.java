/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pnv.api.BookController;

import com.pnv.mappingBook.CategoriesDaoImpl;
import com.pnv.model.Categories;
import java.util.List;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author dorA
 */
@Controller
@RequestMapping(value = "apicate")
public class ApiCategories {

    private CategoriesDaoImpl cateDao;

    @RequestMapping(method = RequestMethod.GET)
    public @ResponseBody List<Categories> viewCateApi() {
        cateDao = new CategoriesDaoImpl();
        List<Categories> cate_list = cateDao.findAll();
        return cate_list;
    }
}
