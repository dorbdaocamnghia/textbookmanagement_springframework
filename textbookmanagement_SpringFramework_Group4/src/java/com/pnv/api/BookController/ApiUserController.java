/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pnv.api.BookController;

import com.pnv.mappingBook.UserDaoImpl;
import com.pnv.model.User;
import java.util.List;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author nghiabl
 */
@Controller
@RequestMapping(value = "apiuser")
public class ApiUserController {
    private UserDaoImpl userDao;

    @RequestMapping(method = RequestMethod.GET)
    public @ResponseBody List<User> viewUserApi() {
        userDao = new UserDaoImpl();
        List<User> cate_list = userDao.findAll();
        return cate_list;
    }
}
