/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pnv.mappingBook;

import com.pnv.model.Bookclass;
import java.util.List;

/**
 *
 * @author dorA
 */
public interface IBookclassDao {
    public void saveOrUpdate(Bookclass bookclass);

    public void delete(Bookclass bookclass);

    public List<Bookclass> findAll();

    public Bookclass findByBookclassId(String id);

    public Bookclass findByBookclassCode(String bookclassCode);
}
