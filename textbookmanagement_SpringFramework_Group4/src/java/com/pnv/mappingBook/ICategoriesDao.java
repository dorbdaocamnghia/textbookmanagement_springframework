/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pnv.mappingBook;

import com.pnv.model.Categories;
import java.util.List;

/**
 *
 * @author dorA
 */
public interface ICategoriesDao {
    public void saveOrUpdate(Categories cate);

    public void delete(Categories cate);

    public List<Categories> findAll();

    public Categories findByTitleId(String cateId);

    public Categories findByTitleCode(String cateCode);
}
