/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pnv.mappingBook;

import com.pnv.model.User;
import java.util.List;

/**
 *
 * @author bdaoy
 */
public interface UserDao {

    public void saveOrUpdate(User user);
    public User findByUserId(String id);

    public List<User> findAll();

    public boolean checkUser(String username, String password);
}
