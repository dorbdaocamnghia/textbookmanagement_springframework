/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pnv.mappingBook;

import com.pnv.model.Bookclass;
import com.pnv.util.BookUtil;
import java.util.List;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;


public class BookclassDaoImpl implements IBookclassDao {
     private final SessionFactory sessionFactory = BookUtil.getSessionFactory();
    @Override
    public void saveOrUpdate(Bookclass bookclass) {
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        try {
            session.saveOrUpdate(bookclass);
            transaction.commit();
        } catch (HibernateException hb) {
            transaction.rollback();
            System.err.println("error" + hb);
        } finally {
            session.close();
        }
    }

    @Override
    public void delete(Bookclass bookclass) {
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        try {
            session.delete(bookclass);
            transaction.commit();
        } catch (HibernateException hb) {
            transaction.rollback();
            System.err.println("error" + hb);
        } finally {
            session.close();
        }
    }

    @Override
    public List<Bookclass> findAll() {
        Session session = sessionFactory.openSession();
        List<Bookclass> bookclass = session.createQuery("from Bookclass").list();
        session.close();
        return bookclass;
    }

    @Override
    public Bookclass findByBookclassId(String id) {
        Bookclass bookclass = null;
        Session session = sessionFactory.getCurrentSession();
        Transaction transaction = session.beginTransaction();
        try {
            bookclass = (Bookclass) session.get(Bookclass.class, id);
        } catch (HibernateException hb) {
            transaction.rollback();
            System.err.println("error" + hb);
        } finally {
            session.close();
        }
        return bookclass;
    }

    @Override
    public Bookclass findByBookclassCode(String bookclassCode) {
        Bookclass department = null;
        String strQuery = "from Bookclass WHERE BookClassId = :bookclassCode ";
        Session session = sessionFactory.openSession();
        Query query = session.createQuery(strQuery);
        query.setParameter("bookclassCode", bookclassCode);
        department = (Bookclass) query.uniqueResult();
        session.close();
        return department;
    }
    
    
    
}
