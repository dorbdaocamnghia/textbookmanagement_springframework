/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pnv.mappingBook;

import com.pnv.model.Categories;
import com.pnv.util.BookUtil;
import java.util.List;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

public class CategoriesDaoImpl implements ICategoriesDao {

    private final SessionFactory sessionFactory = BookUtil.getSessionFactory();

    public CategoriesDaoImpl() {
    }

    @Override
    public void saveOrUpdate(Categories cate) {
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        try {
            session.saveOrUpdate(cate);
            transaction.commit();
        } catch (HibernateException hb) {
            transaction.rollback();
            System.err.println("error" + hb);
        } finally {
            session.close();
        }
    }

    @Override
    public void delete(Categories cate) {

        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        try {
            session.delete(cate);
//            session.flush();
            transaction.commit();
        } catch (HibernateException hb) {
            transaction.rollback();
            System.err.println("error" + hb);
        } finally {
            session.close();
        }
    }

    @Override
    public List<Categories> findAll() {
         Session session = sessionFactory.openSession();
        List<Categories> catelist = session.createQuery("from Categories").list();
        session.close();
        return catelist;
    }

    @Override
    public Categories findByTitleId(String cateId) {
        Categories cate = null;
        Session session = sessionFactory.getCurrentSession();
         Transaction transaction = session.beginTransaction();
        try {
          cate =  (Categories) session.get(Categories.class, cateId);
        } catch (HibernateException hb) {
            transaction.rollback();
            System.err.println("error" + hb);
        } finally {
            session.close();
        }
        return cate;
    }

    @Override
    public Categories findByTitleCode(String cateCode) {
        Categories cate = null;
        String strQuery = "from Categories WHERE cateId =:cateCode ";
        Session session = sessionFactory.openSession();
        Query query = session.createQuery(strQuery);
        query.setParameter("cateCode", cateCode);
        cate = (Categories) query.uniqueResult();
        session.close();
        return cate;
    }
}
