/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pnv.mappingBook;

import com.pnv.model.Books;
import com.pnv.util.BookUtil;
import java.util.List;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

public class BookDaoImpl implements IBookDao {

    private final SessionFactory sessionFactory = BookUtil.getSessionFactory();
    int count=0;

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }
    @Override
    public void saveOrUpdate(Books book) {
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        try {
            session.saveOrUpdate(book);
            transaction.commit();
        } catch (HibernateException hb) {
            transaction.rollback();
            System.err.println("error" + hb);
        } finally {
            session.close();
        }
    }

    @Override
    public void delete(Books book) {
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        try {
            session.delete(book);
            transaction.commit();
        } catch (HibernateException hb) {
            transaction.rollback();
            System.err.println("error" + hb);
        } finally {
            session.close();
        }
    }

    @Override
    public List<Books> findAll() {
        Session session = sessionFactory.openSession();
        List<Books> bookList = session.createQuery("from Books").list();
        session.close();
        this.setCount(bookList.size());
        return bookList;
    }

    @Override
    public Books findByBooktId(String id) {
        Books book = null;
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        try {
            book = (Books) session.get(Books.class, id);
        } catch (HibernateException hb) {
            transaction.rollback();
            System.err.println("error" + hb);
        } finally {
            session.close();
        }
        return book;
    }

    @Override
    public List<Books> findByBookIdClass(String id) {
        Session session = sessionFactory.openSession();
        String hql = "FROM Books WHERE bookClassID =:dor ";
        Query query = session.createQuery(hql);
        query.setParameter("dor", id);
        List results = query.list();
        this.setCount(results.size());
        return results;
    }
    public List<Books> searchByClass(String className) {
        String strQuery = "from Books WHERE bookClassId LIKE :className ";
        Session session = sessionFactory.openSession();
        Query query = session.createQuery(strQuery);
        query.setParameter( "className", "%" + className + "%" );
        List<Books> book_list = query.list();
        this.setCount(book_list.size());
        session.close();
        return book_list;
    }
    public List<Books> searchByID(String className) {
        String strQuery = "from Books WHERE bookId LIKE :className ";
        Session session = sessionFactory.openSession();
        Query query = session.createQuery(strQuery);
        query.setParameter( "className", "%" + className + "%" );
        List<Books> book_list = query.list();
        this.setCount(book_list.size());
        session.close();
        return book_list;
    }
    public List<Books> searchByName(String className) {
        String strQuery = "from Books WHERE bookName LIKE :className ";
        Session session = sessionFactory.openSession();
        Query query = session.createQuery(strQuery);
        query.setParameter( "className", "%" + className + "%" );
        List<Books> book_list = query.list();
        this.setCount(book_list.size());
        session.close();
        return book_list;
    }
}
