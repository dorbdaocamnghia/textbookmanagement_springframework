/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pnv.mappingBook;

import com.pnv.model.Books;
import java.util.List;

/**
 *
 * @author dorA
 */
public interface IBookDao {
     public void saveOrUpdate(Books book);

    public void delete(Books book);

    public List<Books> findAll();

    public Books findByBooktId(String id);

    public List<Books> findByBookIdClass(String bookCode);
}
