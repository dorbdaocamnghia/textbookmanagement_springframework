/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pnv.mappingBook;
import com.pnv.model.User;
import com.pnv.util.BookUtil;
import java.util.List;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

/**
 *
 * @author bdaoy
 */
public class UserDaoImpl implements UserDao {

    private final SessionFactory sessionFactory = BookUtil.getSessionFactory();

    public UserDaoImpl() {
    }

    @Override
    public void saveOrUpdate(User user) {
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        try {
            session.saveOrUpdate(user);
            transaction.commit();
        } catch (HibernateException hb) {
            transaction.rollback();
            System.err.println("error" + hb);
        } finally {
            session.close();
        }

    }

    @Override
    public List<User> findAll() {
        Session session = sessionFactory.openSession();
        List<User> user = session.createQuery("from User").list();
        session.close();
        return user;
    }

    @Override
    public boolean checkUser(String tenDangNhap, String passwordDN) {
        Session session = sessionFactory.openSession();
        List<User> user = session.createQuery("from User").list();
        User us = new User();
        session.close();
        for (int i = 0; i < user.size(); i++) {
            us = user.get(i);
            if (us.getPassword().toString().equals(passwordDN) && us.getUsername().toString().equals(tenDangNhap)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public User findByUserId(String id) {
        User user = null;
        Session session = BookUtil.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        try {
            user = (User) session.get(User.class, id);
        } catch (HibernateException hb) {
            transaction.rollback();
            System.err.println("error" + hb);
        } finally {
            session.close();
        }
        return user;
    }
}
