/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pnv.util;

import com.pnv.model.Books;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

/**
 *
 * @author dorA
 */
public class BookValidation implements Validator {

    @Override
    public boolean supports(Class<?> type) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void validate(Object target, Errors errors) {
        Books bcl  = (Books) target;
        if (bcl.getBookclass().getBookClassId().equals("")) {
            errors.rejectValue("bookclass", "class_.id.wrong");
        }
        if(bcl.getCategories().getCateId().equals("")) {
            errors.rejectValue("categories", "cate.id.wrong");
        }
        if (bcl.getQuanlity()<=0) {
            errors.rejectValue("quanlity", "quantity.mun.wrong");
        }
        if (bcl.getPrice()<0) {
            errors.rejectValue("price", "price.p.wrong");
        }
        if (bcl.getPublishingYear().length() > 4 || bcl.getPublishingYear().length() < 4) {
            errors.rejectValue("publishingYear", "publishingYear.p.wrong");
        }
    }
}
