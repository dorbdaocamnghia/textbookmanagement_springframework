package com.pnv.model;
// Generated May 26, 2015 2:20:25 PM by Hibernate Tools 3.2.1.GA


import java.util.HashSet;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * Bookclass generated by hbm2java
 */
@Entity
@Table(name="bookclass"
    ,catalog="bookmanagement_bcdn"
)
public class Bookclass  implements java.io.Serializable {


     private String bookClassId;
     private String bookclassName;
     private Set bookses = new HashSet(0);

    public Bookclass() {
    }

	
    public Bookclass(String bookClassId, String bookclassName) {
        this.bookClassId = bookClassId;
        this.bookclassName = bookclassName;
    }
    public Bookclass(String bookClassId, String bookclassName, Set bookses) {
       this.bookClassId = bookClassId;
       this.bookclassName = bookclassName;
       this.bookses = bookses;
    }
   
     @Id 
    
    @Column(name="bookClassId", unique=true, nullable=false, length=10)
    public String getBookClassId() {
        return this.bookClassId;
    }
    
    public void setBookClassId(String bookClassId) {
        this.bookClassId = bookClassId;
    }
    
    @Column(name="bookclassName", nullable=false, length=50)
    public String getBookclassName() {
        return this.bookclassName;
    }
    
    public void setBookclassName(String bookclassName) {
        this.bookclassName = bookclassName;
    }
@OneToMany(cascade=CascadeType.ALL, fetch=FetchType.LAZY, mappedBy="bookclass")
    public Set getBookses() {
        return this.bookses;
    }
    
    public void setBookses(Set bookses) {
        this.bookses = bookses;
    }




}


