<%-- 
    Document   : newjspbook-add
    Created on : May 16, 2015, 5:30:48 PM
    Author     : dorA
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <div style="width: 100%; height: 100%; border: 1px solid black; margin-top: -8px;">
            <%@ include file="./header.jsp" %>
            <div style="margin-top: 23px;  margin-left: 13px; border: 1px solid">
                <table>
                    <form:form action="save" modelAttribute="bookForm" method="POST" >
                        <tr>
                            <td align="left" width="20%">Book ID: </td>
                            <td align="left" width="40%"><form:input path="bookId" size="30"/></td>
                            <td align="left" style="color: red"><form:errors path="bookId" cssClass="error"/></td>
                        </tr>
                        <tr>
                            <td>Book Name: </td>
                            <td><form:input path="bookName" size="30"/></td>
                            <td align="left" style="color: red"><form:errors path="bookName" cssClass="error"/></td>
                        </tr>
                        <tr>
                            <td>Categories: </td>
                            <td><form:select path="categories.cateId">
                                    <form:option value="" label="--Please Select"/>
                                    <form:options items="${cate_list}" itemValue="cateId" itemLabel="cateName"/>
                                </form:select>
                            </td>
                            <td align="left" style="color: red"><form:errors path="categories" cssClass="error"/></td>
                        </tr>
                        <tr>
                            <td>Book Class </td>
                            <td><form:select path="bookclass.bookClassId">
                                    <form:option value="" label="--Please Select"/>
                                    <form:options items="${bookclass_list}" itemValue="bookClassId" itemLabel="bookclassName"/>
                                </form:select>
                            </td>
                            <td align="left" style="color: red"><form:errors path="bookclass" cssClass="error"/></td>
                        </tr>
                        <tr>
                            <td>Author: </td>
                            <td><form:input path="author" size="30"/></td>
                            <td align="left"><form:errors path="author" cssClass="error"/></td>
                        </tr>
                        <tr>
                            <td>Quantity: </td>
                            <td><form:input path="quanlity" type="number" size="30"/></td>
                            <td align="left" style="color: red"><form:errors path="quanlity" cssClass="error"/></td>
                        </tr>
                        <tr>
                            <td>Price: </td>
                            <td><form:input path="price" type="number" size="30"/></td>
                            <td align="left" style="color: red"><form:errors path="price" cssClass="error"/></td>
                        </tr>
                        <tr>
                            <td>Publisher: </td>
                            <td><form:input path="publisher" size="30"/></td>
                        </tr>
                        <tr>
                            <td>Publishing Year </td>
                            <td><form:input path="publishingYear" type = "number" size="30"/></td>
                            <td align="left" style="color: red"><form:errors path="publishingYear" cssClass="error"/></td>
                        </tr>
                        <tr>
                            <td align="right"><a href="<%=request.getContextPath()%>/book/"/><input type="button" value="Back"/></a></td>
                            <td align="l"><input type="submit" value="submit"/></td>
                            <td></td>
                        </tr>
                    </form:form>
                </table>
            </div>
            <%@include file="./footer.jsp" %>
        </div>
    </body>
</html>
