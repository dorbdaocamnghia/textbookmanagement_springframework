<%-- 
    Document   : phanTrang
    Created on : May 24, 2015, 8:20:02 AM
    Author     : dorA
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link href="<c:url value="/resources/styles/smartpaginator.css" />" rel="stylesheet" type="text/css" />
        <script type='text/javascript' src="<c:url value="/resources/js/jquery-1.4.4.min.js" />" ></script>
        <script type='text/javascript' src="<c:url value="/resources/js/smartpaginator.js" />" ></script>
        <script type="text/javascript">
            $(document).ready(function() {

                $('ul li').click(function() {

                    $('#green-contents').css('display', 'none');

                    if ($(this).attr('id') == '2')
                        $('#green-contents').css('display', '');

                });

                $('#green').smartpaginator({totalrecords: ${num.n}, recordsperpage: 10, datacontainer: 'mt', dataelement: 'tr', initval: 0, next: 'Next', prev: 'Prev', first: 'First', last: 'Last', theme: 'green'});

                $('#black').smartpaginator({totalrecords: 5, recordsperpage: 1, datacontainer: 'divs', dataelement: 'div', initval: 0, next: 'Next', prev: 'Prev', first: 'First', last: 'Last', theme: 'black'});

                $('#red').smartpaginator({totalrecords: 32, recordsperpage: 4, length: 4, next: 'Next', prev: 'Prev', first: 'First', last: 'Last', theme: 'red', controlsalways: true, onchange: function(newPage) {
                        $('#r').html('Page # ' + newPage);
                    }
                });

            });
        </script>
        <style type="text/css">

            #wrapper
            {
                margin: auto;
                width: 100%;
            }
            .contents
            {
                width: 91%; /*height: 150px;*/
                margin: 0;
            }
            .contents > p
            {
                padding: 8px;
            }
            .table
            {
                width: 100%;
                border-right: solid 1px #5f9000;
            }
            .table th
            {
                border-left: solid 1px #5f9000;
                height: 50px;
            }
            .table td
            {
                border-left: solid 1px #5f9000;
                border-bottom: solid 1px #5f9000;
                height: 50px;
            }
            .header
            {
                background-color: #4f7305;
                color: White;
            }
            #divs
            {
                margin: 0;
                height: 200px;
                font: verdana;
                font-size: 14px;
                background-color: White;
            }
            #divs > div
            {
                width: 98%;
                padding: 8px;
            }
            #divs > div p
            {
                width: 95%;
                padding: 8px;
            }
            ul.tab
            {
                list-style: none;
                margin: 0;
                padding: 0;
            }
            ul.tab li
            {
                display: inline;
                padding: 10px;
                color: White;
                cursor: pointer;
            }
            #container
            {
                width: 100%;
                border: solid 1px red;
            }
        </style>
    </head>

    <body>
        <div id="wrapper" style="width: 100%; height: 100%; border: 1px solid black; margin-top: -8px;">
            <%@ include file="./header.jsp" %>
            <div id="green-contents" class="contents" style="border: solid 1px #5f9000; margin-left: 12px;  margin-top: 22px">
                <a href="<%=request.getContextPath()%>/book/add"><img src="<c:url value="/resources/img/addNew.png"/>"/></a>
                <table id="mt" cellpadding="0" cellspacing="0" border="0" class="table">
                    <tr class="header">
                        <th style="width: 80px">Book ID</th>
                        <th>Book Name</th>
                        <th>Author</th>
                        <th>Publisher</th>
                        <th>Publishing Year</th>
                        <th>Quantity</th>
                        <th>Prices</th>
                        <th>Book Class</th>
                        <th>Categories</th>
                        <th>Selection</th>
                    </tr>
                    <c:forEach items="${book_list}" var="book_list">  
                        <tr>  
                            <td><c:out value="${book_list.bookId}"/></td>  
                            <td><c:out value="${book_list.bookName}"/></td>
                            <td><c:out value="${book_list.author}"/></td>
                            <td><c:out value="${book_list.publisher}"/></td>  
                            <td><c:out value="${book_list.publishingYear}"/></td>
                            <td><c:out value="${book_list.quanlity}"/></td>
                            <td><c:out value="${book_list.price}"/></td>
                            <td><c:out value="${book_list.bookclass.bookclassName}"/></td>
                            <td><c:out value="${book_list.categories.cateName}"/></td>
                            <td align="center"><a href="<%=request.getContextPath()%>/book/update?id=${book_list.bookId}">Update</a> | 
                                <a href="<%=request.getContextPath()%>/book/delete?id=${book_list.bookId}">Delete</a>
                            </td>
                        </tr>  
                    </c:forEach>
                </table>
                <div id="green" style="margin: auto;">
                </div>
            </div>
                <%@ include file="./footer.jsp" %>
        </div>
        
    </body>
</html>
