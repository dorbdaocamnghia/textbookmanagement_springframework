<%-- 
    Document   : changePass
    Created on : May 24, 2015, 2:55:13 PM
    Author     : bdaoy
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Change password</title>
    </head>
     <style type="text/css">
        .table
        {
            width: 50%;
            border-left:  solid 1px #5f9000;
        }
        .table th
        {
            border-left: solid 1px #5f9000;
            height: 50px;
        }
        .table td
        {
            border-left: solid 1px #5f9000;
            border-bottom: solid 0px #5f9000;
            height: 30px;
        }
        .header
        {
            background-color: #4f7305;
            color: White;
        }
     </style>
    <body>
        <div style="width: 100%; height: 100%; border: 1px solid black; margin-top: -8px;">
            <%@include file="./header.jsp" %>
            <div style="margin-top: 24px;  margin-left: 13px;">
                <table class="table">
                    <tr class="header">
                        <td>Change Password</td>
                        <td></td>
                    </tr>
                    <form:form action="change" modelAttribute="changeForm" method="POST" >
                        <tr>
                            <td align="left" >UserName: </td>
                            <td align="left" ><form:input path="username" size="30" disabled="true" value="${user}"/>
                                <input type="hidden" value="${user}" name="username1"/>
                            </td>

                        </tr>
                        <tr>
                            <td>Password: </td>
                            <td><input type="password" name="oldpass" size="30"/></td>
                            <td style="color: red">${errorOldP}</td>
                        </tr>
                        <tr>
                            <td>New password </td>
                            <td><input type="password" name="newpass" size="30"/></td>
                        </tr>
                        <tr>
                            <td>New password </td>
                            <td><input type="password" name="confirmpassword" size="30"/></td>
                            <td style="color: red">${errorCP}</td>
                        </tr>
                        <tr>
                            <td><button type="submit" class="MyButton" style="width:70px; height: 30px">Change</button>
                                <button type="reset" class="MyButton" style="width:70px; height: 30px">Refresh</button>

                            </td>
                        </tr>
                    </form:form>
                </table>
            </div>
            <%@include file="./footer.jsp" %>
        </div>
    </body>
</html>
