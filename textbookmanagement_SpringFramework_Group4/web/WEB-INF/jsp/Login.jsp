<%-- 
    Document   : Login
    Created on : May 19, 2015, 4:12:17 PM
    Author     : bdaoy
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>

        <style type="text/css">
            #Khung{
                margin: auto;
                margin-top: auto;
                width: 800px;
                height: 500px;
                background-color: #99ccff;

            }
            Body{background-color: #ccccff;}
            #Banner{
                width: 800px;
                height: 50px;
                background-color: #f4ffff;

            }  
            #MenuLeft{
                top: 50px;
                width: 400px;
                height: 450px;
                background-image: url("<c:url value="/resources/img/h2.jpg"/>");
            }
            #MenuRight{
                position: absolute;
                top:32px;
                right: 0px;
                left: 400px;
                width: 400px;
                height: 450px;
                background-color: #f4ffff;
                margin-top: 50px;
                margin-left: 240px;

            }
            #MyButton{

                box-shadow: 0px 1px 0px 0px #f0f7fa;
                background:-webkit-gradient(linear, left top, left bottom, color-stop(0.05, #33bdef), color-stop(1, #019ad2));
                background:-moz-linear-gradient(top, #33bdef 5%, #019ad2 100%);
                background:-webkit-linear-gradient(top, #33bdef 5%, #019ad2 100%);
                background:-o-linear-gradient(top, #33bdef 5%, #019ad2 100%);
                background:-ms-linear-gradient(top, #33bdef 5%, #019ad2 100%);

                filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#33bdef', endColorstr='#019ad2',GradientType=0);
                background-color:#33bdef;

                border:1px solid #057fd0;
                display:inline-block;
                cursor:pointer;
                color:#ffffff;

                padding:6px 24px;
                text-decoration:none;
                text-shadow:0px -1px 0px #5b6178;
            }
            .MyButton:hover {
                background:-webkit-gradient(linear, left top, left bottom, color-stop(0.05, #019ad2), color-stop(1, #33bdef));
                background:-moz-linear-gradient(top, #019ad2 5%, #33bdef 100%);
                background:-webkit-linear-gradient(top, #019ad2 5%, #33bdef 100%);
                background:-o-linear-gradient(top, #019ad2 5%, #33bdef 100%);
                background:-ms-linear-gradient(top, #019ad2 5%, #33bdef 100%);

                filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#019ad2', endColorstr='#33bdef',GradientType=0);
                background-color:#019ad2;
            }
            .MyButton:active {
                position:relative;
                top:1px;
            }


        </style>
    </head>
    <body>
        <div id="Body"></div>
        <div id="Khung">
            <div id="Banner"> <h1><p align="center" style="color:red">Welcome to BCND Book Management System</p></h1></div>
            <div id="MenuLeft">
                <img src="<c:url value="/resources/img/h2.jpg"/>"/>
            </div>
            <div id="MenuRight">
                <div style="text-align: center;">
                    <div style="box-sizing: border-box; display: inline-block; width: auto; max-width: 480px; background-color: #FFFFFF; border: 2px solid #0361A8; border-radius: 5px; box-shadow: 0px 0px 8px #0361A8; margin: 50px auto auto;">
                        <div style="background: #0361A8; border-radius: 5px 5px 0px 0px; padding: 15px;">
                            <span style="font-family: verdana,arial;
                                  color: #D4D4D4; font-size: 1.00em; 
                                  font-weight:bold;">Enter your user Name and password</span></div>

                        <div style="background: #99ccff; padding: 15px">
                            <form:form action="login" method="post" >
                                <table>
                                    <tr>
                                        <td>UserName:</td>
                                        <td><input  type="text" name="username" placeholder="Enter your username"/></td>
                                    </tr>
                                    <tr>
                                        <td>Password:</td>
                                        <td> <input type="password" name="password" placeholder="Enter your password"/></td>
                                    </tr>
                                    <tr>
                                        <td algin="center" colspan="2" style="color: red">${errorLogin}</td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td><button type="submit" class="MyButton" style="width:70px; height: 30px">Login</button>
                                            <button type="reset" class="MyButton" style="width:70px; height: 30px">Refresh</button>
                                            
                                        </td>
                                    </tr>
                                </table>

                            </form:form>

                        </div>

                    </div>


                    </body>
                    </html>
