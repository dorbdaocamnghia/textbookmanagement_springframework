<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Manage Book</title>
    </head>
    <style>
        body {
            background-color: white;
        }

        h1 {
            color: orange;
            text-align: center;
        }

        p {
            font-family: "Times New Roman";
            font-size: 20px;
        }
        #menu{
            margin-top: -6px;
            width: 100%;
            height: 40px;
            margin-left: 13px;
        }
        #formatImage
        {
            height: 63px;
            width: 80px;
        }
    </style>
    <div style="width: 100%; height: 100%; border-radius: 5px; margin-top: -8px">
        <div style="width: 100%; height: 100px; margin-left: 10px;">
            <table style="height: 100%;">
                <tr>
                    <td>
                        <table style="height: 100%; background-color: #F4FFFF; border-radius: 5px " border="1">
                            <tr>
                                <td><a href="<%=request.getContextPath()%>/book/findBook?id=C01"><img src="<c:url value="/resources/img/1.jpg"/>" id="formatImage"/></a></td>
                                <td><a href="<%=request.getContextPath()%>/book/findBook?id=C02"><img src="<c:url value="/resources/img/2.jpg"/>" id="formatImage"/></a></td>
                                <td><a href="<%=request.getContextPath()%>/book/findBook?id=C03"><img src="<c:url value="/resources/img/3.jpg"/>" id="formatImage"/></a></td>
                                <td><a href="<%=request.getContextPath()%>/book/findBook?id=C04"><img src="<c:url value="/resources/img/4.png"/>" id="formatImage"/></a></td>
                                <td><a href="<%=request.getContextPath()%>/book/findBook?id=C05"><img src="<c:url value="/resources/img/5.jpg"/>" id="formatImage"/></a></td>
                            </tr>
                        </table>
                    </td>
                    <td>
                        <table style="height: 100%; background-color: #F4FFFF; border-radius: 5px " border="1">
                            <tr>
                                <td><a href="<%=request.getContextPath()%>/book/findBook?id=C06"><img src="<c:url value="/resources/img/6.jpg"/>" id="formatImage"/></a></td>
                                <td><a href="<%=request.getContextPath()%>/book/findBook?id=C07"><img src="<c:url value="/resources/img/7.jpg"/>" id="formatImage"/></a></td>
                                <td><a href="<%=request.getContextPath()%>/book/findBook?id=C08"><img src="<c:url value="/resources/img/8.jpg"/>" id="formatImage"/></a></td>
                                <td><a href="<%=request.getContextPath()%>/book/findBook?id=C09"><img src="<c:url value="/resources/img/9.jpg"/>" id="formatImage"/></a></td>
                            </tr>
                        </table>
                    </td>
                    <td>
                        <table style="height: 100%;background-color: #F4FFFF; border-radius: 5px " border="1">
                            <tr>
                                <td><a href="<%=request.getContextPath()%>/book/findBook?id=C10"><img src="<c:url value="/resources/img/10.jpg"/>" id="formatImage"/></a></td>
                                <td><a href="<%=request.getContextPath()%>/book/findBook?id=C11"><img src="<c:url value="/resources/img/11.jpg"/>" id="formatImage"/></a></td>
                                <td><a href="<%=request.getContextPath()%>/book/findBook?id=C12"><img src="<c:url value="/resources/img/12.jpg"/>" id="formatImage"/></a></td> 
                            </tr>
                        </table>
                    </td>
                    <td align="right"><a href="<%=request.getContextPath()%>/change">Change your password</a></td>
                </tr>
            </table>
        </div>
        <div id="menu">
            <table border="1" style="height: 100%;">
                <tr>
                    <td style="width: 120px;height:50px;">
                        <a href="<%=request.getContextPath()%>/book/"><img src="<c:url value="/resources/img/h.jpg"/>" style="width: 100px; height: 50px"/></a>
                    </td>
                    <td><a href="<%=request.getContextPath()%>/cate/"><img src="<c:url value="/resources/img/cate.png"/>" style="width: 100px; height: 50px"/></a></td>
                    <td><a href="<%=request.getContextPath()%>/class/"><img src="<c:url value="/resources/img/class.png"/>" style="width: 100px; height: 50px"/></a></td>
                    <td align="right" style="width: 400px;">
                        <form:form action="${pageContext.request.contextPath}/book/seach"  method="POST">
                            <select name="select">
                                <option value="id"/>ID
                                <option value="class"/> Class
                                <option value="name"/>Name
                            </select>
                            <input type="text" name="search"/>
                            <input type="Submit" value="Search">
                        </form:form>
                    </td>
                </tr>
            </table>
        </div>
    </div>
    <body>  
    </body>
</html>
