<%-- 
    Document   : homeClass
    Created on : May 19, 2015, 11:03:36 AM
    Author     : dorA
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Home Class</title>
    </head>
    <style type="text/css">
        .table
        {
            width: 50%;
            border-right: solid 1px #5f9000;
        }
        .table th
        {
            border-left: solid 1px #5f9000;
            height: 50px;
        }
        .table td
        {
            border-left: solid 1px #5f9000;
            border-bottom: solid 1px #5f9000;
            height: 30px;
        }
        .header
        {
            background-color: #4f7305;
            color: White;
        }
    </style>
    <body>
        <div style="width: 100%; height: 100%; border: 1px solid black; margin-top: -8px;">
            <%@ include file="./header.jsp" %>
            <div style="margin-top: 24px;  margin-left: 13px;">
                <a href="<%=request.getContextPath()%>/cate/add"><img src="<c:url value="/resources/img/addNewCate.png"/>"/></a>
                <table class="table">
                    <tr class="header">
                        <th>Categories ID</th>
                        <th>Categories Name</th>
                    </tr>
                    <c:forEach items="${cate_list}" var="cate_list">  
                        <tr>  
                            <td><c:out value="${cate_list.cateId}"/></td>
                            <td><c:out value="${cate_list.cateName}"/></td>
                        </tr>  
                    </c:forEach>
                </table>
            </div>
            <%@include file="./footer.jsp" %>
        </div>

    </body>
</html>
